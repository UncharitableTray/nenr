use fuzzy::domain::SimpleDomain;
use fuzzy::domain::{Domain, Element};
use fuzzy::errors::FuzzyError;
use fuzzy::fset::{FuzzySet, MutableFset};
use fuzzy::relations::*;

type Result<T> = std::result::Result<T, FuzzyError>;

fn main() -> Result<()> {
    let u1 = SimpleDomain::new(1, 5)?;
    let u2 = SimpleDomain::new(1, 4)?;
    let u3 = SimpleDomain::new(1, 5)?;

    let mut r1 = MutableFset::new(Domain::combine(&u1, &u2));
    r1.set_value(&Element::from_vec(vec![1, 1]), 0.3)?;
    r1.set_value(&Element::from_vec(vec![1, 2]), 1.0)?;
    r1.set_value(&Element::from_vec(vec![3, 3]), 0.5)?;
    r1.set_value(&Element::from_vec(vec![4, 3]), 0.5)?;

    for i in Domain::combine(&u1, &u2) {
        println!("{}", i);
    }

    let mut r2 = MutableFset::new(Domain::combine(&u2, &u3));
    r2.set_value(&Element::from_vec(vec![1, 1]), 1.0)?;
    r2.set_value(&Element::from_vec(vec![2, 1]), 0.5)?;
    r2.set_value(&Element::from_vec(vec![2, 2]), 0.7)?;
    r2.set_value(&Element::from_vec(vec![3, 3]), 1.0)?;
    r2.set_value(&Element::from_vec(vec![3, 4]), 0.4)?;

    println!("\nPrinting second!:");
    for i in Domain::combine(&u2, &u3) {
        println!("{}", i);
    }
    println!("");

    let r1r2 = compose_binary(&r1, &r2)?;
    for x in r1r2.get_domain().clone() {
        println!("mu({})={}", x, r1r2.get_val_at(&x).unwrap());
    }
    Ok(())
}
