use fuzzy::domain::SimpleDomain;
use fuzzy::domain::{Domain, Element};
use fuzzy::errors::FuzzyError;
use fuzzy::fset::MutableFset;
use fuzzy::relations::*;

type Result<T> = std::result::Result<T, FuzzyError>;

fn main() -> Result<()> {
    let u = SimpleDomain::new(1, 6).unwrap();
    let u2 = Domain::combine(&u, &u);

    let mut r1 = MutableFset::new(u2.clone());
    r1.set_value(&Element::from_vec(vec![1, 1]), 1.0).unwrap();
    r1.set_value(&Element::from_vec(vec![2, 2]), 1.0).unwrap();
    r1.set_value(&Element::from_vec(vec![3, 3]), 1.0).unwrap();
    r1.set_value(&Element::from_vec(vec![4, 4]), 1.0).unwrap();
    r1.set_value(&Element::from_vec(vec![5, 5]), 1.0).unwrap();
    r1.set_value(&Element::from_vec(vec![3, 1]), 0.5).unwrap();
    r1.set_value(&Element::from_vec(vec![1, 3]), 0.5).unwrap();

    let mut r2 = MutableFset::new(u2.clone());
    r2.set_value(&Element::from_vec(vec![1, 1]), 1.0).unwrap();
    r2.set_value(&Element::from_vec(vec![2, 2]), 1.0).unwrap();
    r2.set_value(&Element::from_vec(vec![3, 3]), 1.0).unwrap();
    r2.set_value(&Element::from_vec(vec![4, 4]), 1.0).unwrap();
    r2.set_value(&Element::from_vec(vec![5, 5]), 1.0).unwrap();
    r2.set_value(&Element::from_vec(vec![3, 1]), 0.5).unwrap();
    r2.set_value(&Element::from_vec(vec![1, 3]), 0.1).unwrap();

    let mut r3 = MutableFset::new(u2.clone());
    r3.set_value(&Element::from_vec(vec![1, 1]), 1.0).unwrap();
    r3.set_value(&Element::from_vec(vec![2, 2]), 1.0).unwrap();
    r3.set_value(&Element::from_vec(vec![3, 3]), 0.3).unwrap();
    r3.set_value(&Element::from_vec(vec![4, 4]), 1.0).unwrap();
    r3.set_value(&Element::from_vec(vec![5, 5]), 1.0).unwrap();
    r3.set_value(&Element::from_vec(vec![1, 2]), 0.6).unwrap();
    r3.set_value(&Element::from_vec(vec![2, 1]), 0.6).unwrap();
    r3.set_value(&Element::from_vec(vec![2, 3]), 0.7).unwrap();
    r3.set_value(&Element::from_vec(vec![3, 2]), 0.7).unwrap();
    r3.set_value(&Element::from_vec(vec![3, 1]), 0.5).unwrap();
    r3.set_value(&Element::from_vec(vec![1, 3]), 0.5).unwrap();

    let mut r4 = MutableFset::new(u2.clone());
    r4.set_value(&Element::from_vec(vec![1, 1]), 1.0).unwrap();
    r4.set_value(&Element::from_vec(vec![2, 2]), 1.0).unwrap();
    r4.set_value(&Element::from_vec(vec![3, 3]), 1.0).unwrap();
    r4.set_value(&Element::from_vec(vec![4, 4]), 1.0).unwrap();
    r4.set_value(&Element::from_vec(vec![5, 5]), 1.0).unwrap();
    r4.set_value(&Element::from_vec(vec![1, 2]), 0.4).unwrap();
    r4.set_value(&Element::from_vec(vec![2, 1]), 0.4).unwrap();
    r4.set_value(&Element::from_vec(vec![2, 3]), 0.5).unwrap();
    r4.set_value(&Element::from_vec(vec![3, 2]), 0.5).unwrap();
    r4.set_value(&Element::from_vec(vec![3, 1]), 0.4).unwrap();
    r4.set_value(&Element::from_vec(vec![1, 3]), 0.4).unwrap();

    let test1 = is_u_times_u_relation(&r1)?;
    println!("r1 je definiran nad UxU? {}", test1);

    let test2 = is_symmetric(&r1)?;
    println!("r1 je simetricna? {}", test2);

    let test3 = is_symmetric(&r2)?;
    println!("r2 je simetricna? {}", test3);

    let test4 = is_reflexive(&r1)?;
    println!("r1 je refleksivna? {}", test4);

    let test5 = is_reflexive(&r3)?;
    println!("r3 je refleksivna? {}", test5);

    let test6 = is_max_min_transitive(&r3)?;
    println!("r3 je max-min tranzitivna? {}", test6);

    let test7 = is_max_min_transitive(&r4)?;
    println!("r4 je max-min tranzitivna? {}", test7);
    Ok(())
}
