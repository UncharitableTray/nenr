extern crate fuzzy;

use std::io::prelude::*;
use std::io::{stdin, stdout};

use fuzzy::domain::SimpleDomain;
use fuzzy::fset::CalcFset;
use fuzzy::inference::*;
use fuzzy::ops::unary_ops::*;

fn main() {
    let one = OneFun::new();

    // Distance
    let very_close = LFun::new(10, 200);
    let angled_close = GammaFun::new(10, 200);
    let far_enough = GammaFun::new(100, 1300);
    let l_domain = SimpleDomain::new(0, 1301).unwrap();

    // Velocity
    let slow = LFun::new(1, 25);
    let fast = GammaFun::new(20, 100);
    let v_domain = SimpleDomain::new(0, 1301).unwrap();

    // Accel
    let decelerate = LFun::new(-35, 5);
    let accelerate = GammaFun::new(-5, 35);
    let a_domain = SimpleDomain::new(-35, 36).unwrap();

    // Angles
    let left_turn = LFun::new(-90, 1);
    let right_turn = GammaFun::new(1, 90);
    let straight = LambdaFun::new(-10, 0, 10);
    let k_domain = SimpleDomain::new(-90, 91).unwrap();

    let s_domain = SimpleDomain::new(0, 2).unwrap();

    let a1 = vec![
        CalcFset::new(l_domain.clone(), &very_close),
        CalcFset::new(l_domain.clone(), &one),
        CalcFset::new(l_domain.clone(), &angled_close),
        CalcFset::new(l_domain.clone(), &one),
        CalcFset::new(v_domain.clone(), &one),
        CalcFset::new(s_domain.clone(), &one),
    ];
    let c1 = CalcFset::new(a_domain.clone(), &right_turn);
    let acc_r1 = Rule::new(a1, c1);

    let a1 = vec![
        CalcFset::new(l_domain.clone(), &one),
        CalcFset::new(l_domain.clone(), &very_close),
        CalcFset::new(l_domain.clone(), &one),
        CalcFset::new(l_domain.clone(), &angled_close),
        CalcFset::new(v_domain.clone(), &one),
        CalcFset::new(s_domain.clone(), &one),
    ];
    let c1 = CalcFset::new(k_domain.clone(), &left_turn);
    let acc_r2 = Rule::new(a1, c1);

    let a1 = vec![
        CalcFset::new(l_domain.clone(), &far_enough),
        CalcFset::new(l_domain.clone(), &far_enough),
        CalcFset::new(l_domain.clone(), &far_enough),
        CalcFset::new(l_domain.clone(), &far_enough),
        CalcFset::new(v_domain.clone(), &one),
        CalcFset::new(s_domain.clone(), &one),
    ];
    let c1 = CalcFset::new(a_domain.clone(), &straight);
    let acc_r3 = Rule::new(a1, c1);

    let a1 = vec![
        CalcFset::new(l_domain.clone(), &very_close),
        CalcFset::new(l_domain.clone(), &one),
        CalcFset::new(l_domain.clone(), &angled_close),
        CalcFset::new(l_domain.clone(), &one),
        CalcFset::new(v_domain.clone(), &fast),
        CalcFset::new(s_domain.clone(), &one),
    ];
    let c1 = CalcFset::new(a_domain.clone(), &decelerate);
    let ang_r1 = Rule::new(a1, c1);

    let a1 = vec![
        CalcFset::new(l_domain.clone(), &one),
        CalcFset::new(l_domain.clone(), &very_close),
        CalcFset::new(l_domain.clone(), &one),
        CalcFset::new(l_domain.clone(), &angled_close),
        CalcFset::new(v_domain.clone(), &fast),
        CalcFset::new(s_domain.clone(), &one),
    ];
    let c1 = CalcFset::new(a_domain.clone(), &decelerate);
    let ang_r2 = Rule::new(a1, c1);

    let a1 = vec![
        CalcFset::new(l_domain.clone(), &far_enough),
        CalcFset::new(l_domain.clone(), &far_enough),
        CalcFset::new(l_domain.clone(), &far_enough),
        CalcFset::new(l_domain.clone(), &far_enough),
        CalcFset::new(v_domain.clone(), &slow),
        CalcFset::new(s_domain.clone(), &one),
    ];
    let c1 = CalcFset::new(a_domain.clone(), &accelerate);
    let ang_r3 = Rule::new(a1, c1);

    let xs = vec![15, 15, 15, 15, 1, 1];
    let con_acc = MinimumController::new(xs.clone(), vec![acc_r1, acc_r2, acc_r3]);
    let con_ang = MinimumController::new(xs.clone(), vec![ang_r1, ang_r2, ang_r3]);

    loop {
        let mut nums = String::new();
        stdin()
            .read_line(&mut nums)
            .expect("Failed to get numbers.");
        let xs = nums
            .split_whitespace()
            .map(|s| s.trim().parse::<i32>().expect("AAAA"))
            .collect::<Vec<i32>>();
        let a = con_acc.infer(&xs);
        let k = con_ang.infer(&xs);
        eprintln!("{} {}", a, k);
        println!("{} {}", a, k);
        stdout().flush().unwrap();
    }
}
