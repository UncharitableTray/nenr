extern crate nnet;

use std::fs::File;
use std::io::Write;

use nnet::anfis::ANFIS;
use nnet::util::create_anfis_examples;

fn main() {
    write_dataset(&"reports/anfis/3d/3d_function_plot.txt");
    write_domain_and_errors(
        1,
        1,
        &"reports/anfis/3d/3d_function_1_rule_stochastic.txt",
        &"reports/anfis/3d/3d_errors_1_rule_stochastic.txt",
    );

    write_domain_and_errors(
        1,
        81,
        &"reports/anfis/3d/3d_function_1_rule_batch.txt",
        &"reports/anfis/3d/3d_errors_1_rule_batch.txt",
    );

    write_domain_and_errors(
        2,
        1,
        &"reports/anfis/3d/3d_function_2_rules_stochastic.txt",
        &"reports/anfis/3d/3d_errors_2_rules_stochastic.txt",
    );

    write_domain_and_errors(
        2,
        81,
        &"reports/anfis/3d/3d_function_2_rules_batch.txt",
        &"reports/anfis/3d/3d_errors_2_rules_batch.txt",
    );

    write_domain_and_errors(
        5,
        1,
        &"reports/anfis/3d/3d_function_5_rules_stochastic.txt",
        &"reports/anfis/3d/3d_errors_5_rules_stochastic.txt",
    );

    write_domain_and_errors(
        5,
        81,
        &"reports/anfis/3d/3d_function_5_rules_batch.txt",
        &"reports/anfis/3d/3d_errors_5_rules_batch.txt",
    );
    //train_anfis(5, 100000, 81, 0.0005, false, false);
}

fn train_anfis(
    k: usize,
    iters: i32,
    batch_size: usize,
    learning_rate: f64,
    error_trace: bool,
    print_iters: bool,
) -> (ANFIS, Option<Vec<f64>>) {
    let dataset = create_anfis_examples();
    let mut net = ANFIS::new(dataset, k);

    let mut errs = Vec::new();
    for i in -1..iters {
        let e = net.epoch(Some(batch_size), learning_rate, error_trace);
        if error_trace {
            errs.push(e.unwrap())
        }
        if print_iters && ((i + 1) % 10000 == 0) {
            println!("Iter {} - total MSE {}", i + 1, net.total_mse());
        }
    }
    if error_trace {
        return (net, Some(errs));
    }
    (net, None)
}

fn write_domain_and_errors(
    rules: usize,
    batch_size: usize,
    fn_file_path: &str,
    err_file_path: &str,
) {
    let dataset = create_anfis_examples();
    let (mut net, _) = train_anfis(rules, 100000, batch_size, 0.001, false, true);

    let mut function_file = File::create(fn_file_path).expect("Unable to create function file");
    let mut error_file = File::create(err_file_path).expect("Unable to create error file");

    for e in dataset {
        let predicted = net.forward_propagate(&e);

        let mut function_string = Vec::new();
        writeln!(&mut function_string, "{} {} {}", e.0[0], e.0[1], predicted).unwrap();
        function_file
            .write(&function_string)
            .expect("Unable to write to function file.");

        let mut error_string = Vec::new();
        writeln!(
            &mut error_string,
            "{} {} {}",
            e.0[0],
            e.0[1],
            e.1 - predicted
        )
        .unwrap();
        error_file
            .write(&error_string)
            .expect("Unable to write to error file.");
    }
}

fn write_dataset(path: &str) {
    let dataset = create_anfis_examples();
    let mut file = File::create(path).expect("Error creating anfis_examples.txt");
    for e in dataset {
        let mut string = Vec::new();
        writeln!(&mut string, "{} {} {}", e.0[0], e.0[1], e.1).unwrap();
        file.write(&string)
            .expect("Unable to write in anfis_examples.txt");
    }
}
