extern crate fuzzy;

use fuzzy::domain::Element;
use fuzzy::domain::SimpleDomain;
use fuzzy::fset::MutableFset;
use fuzzy::ops::binary_ops::*;
use fuzzy::ops::{real_binary_operate, real_unary_operate};
use fuzzy::util::print_set;

fn main() {
    let d1 = SimpleDomain::new(0, 11).unwrap();
    let mut s1 = MutableFset::new(d1);
    s1.set_value(&Element::from_vec(vec![0]), 1.0).unwrap();
    s1.set_value(&Element::from_vec(vec![1]), 0.8).unwrap();
    s1.set_value(&Element::from_vec(vec![2]), 0.6).unwrap();
    s1.set_value(&Element::from_vec(vec![3]), 0.4).unwrap();
    s1.set_value(&Element::from_vec(vec![4]), 0.2).unwrap();
    println!("set1");
    print_set(&s1);

    let zadeh_not = ZadehNot {};
    let not_s1 = real_unary_operate(&s1, &zadeh_not);
    println!("not_set1");
    print_set(&not_s1);

    let zadeh_or = ZadehOr {};
    let union = real_binary_operate(&s1, &not_s1, &zadeh_or);
    println!("set1 union not_set1");
    print_set(&union);

    let hamacher_t = HamacherTNorm { param: 1.0 };
    let intersection = real_binary_operate(&s1, &not_s1, &hamacher_t);
    println!("set1 intersect not_set1 using Hamacher T-Norm with param 1.0");
    print_set(&intersection);
}
