use crate::error::MathError;
use crate::matrix::Matrix;
use crate::PRECISION;

use std::fmt;

type Result<T> = std::result::Result<T, MathError>;

pub trait Vector {
    fn get_elem(&self, index: usize) -> Option<f64>;

    fn set_elem(&mut self, index: usize, val: f64) -> Option<()>;

    fn get_dim(&self) -> usize;

    fn clone(&self) -> Self;

    fn clone_part(&self, n: usize) -> Self;

    fn add(&self, other: &Self) -> Result<Self>
    where
        Self: std::marker::Sized;

    fn add_inplace(&mut self, other: &Self) -> Result<()>;

    fn sub(&self, other: &Self) -> Result<Self>
    where
        Self: std::marker::Sized;

    fn sub_inplace(&mut self, other: &Self) -> Result<()>;

    fn scale(&self, f: f64) -> Self;

    fn scale_inplace(&mut self, f: f64);

    fn norm(&self) -> f64;

    fn normalize(&self) -> Self;

    fn normalize_inplace(&mut self);

    fn cosine(&self, other: &Self) -> f64;

    fn dot(&self, other: &Self) -> f64;

    fn cross(&self, other: &Self) -> Result<Self>
    where
        Self: std::marker::Sized;

    fn from_homogeneous(&self) -> Result<Self>
    where
        Self: std::marker::Sized;

    fn elems(&self) -> &Vec<f64>;

    fn elems_mut(&mut self) -> &mut Vec<f64>;

    fn from_row_matrix(m: &impl Matrix) -> Result<Self>
    where
        Self: std::marker::Sized;

    fn from_col_matrix(m: &impl Matrix) -> Result<Self>
    where
        Self: std::marker::Sized;
}

#[derive(Debug)]
pub struct RealVec {
    vals: Vec<f64>,

    dim: usize,

    mutable: bool,
}

impl RealVec {
    /// Creates a mutable RealVec, consuming a Vec in the process.
    pub fn from_vec(vals: Vec<f64>) -> RealVec {
        let len = vals.len();
        RealVec {
            vals: vals,
            dim: len,
            mutable: true,
        }
    }

    /// Creates a RealVec which is either Read-Only or not. Also consumes a Vec in the process.
    pub fn from_vec_custom(mutable: bool, vals: Vec<f64>) -> RealVec {
        let len = vals.len();
        let m = mutable;
        RealVec {
            vals: vals,
            dim: len,
            mutable: m,
        }
    }

    pub fn from_real_vec(other: &RealVec) -> RealVec {
        let vals = other.values().clone();
        let dim = other.get_dim();
        RealVec {
            vals: vals,
            dim: dim,
            mutable: true,
        }
    }

    pub fn values(&self) -> &Vec<f64> {
        &self.vals
    }

    pub fn values_mut(&mut self) -> &mut Vec<f64> {
        &mut self.vals
    }

    fn check_dimensions(&self, other: &RealVec) -> Result<()> {
        if self.dim != other.get_dim() {
            return Err(MathError::DimensionError);
        }
        Ok(())
    }
}

impl PartialEq for RealVec {
    fn eq(&self, other: &RealVec) -> bool {
        self.dim == other.dim
            && self
                .vals
                .iter()
                .zip(other.values().iter())
                .all(|(a, b)| (a - b).abs() < PRECISION)
    }
}

impl Eq for RealVec {}

impl fmt::Display for RealVec {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", &self.vals[..])
    }
}

impl Vector for RealVec {
    /// Gets the element of the vector denoted by the index
    ///
    /// # Examples
    /// ```
    /// use linalg::vector::Vector;
    /// use linalg::vector::RealVec;
    ///
    /// let a = RealVec::from_vec(vec![1.0, 2.0, 3.0]);
    /// let x = a.get_elem(1).unwrap();
    /// assert_eq!(x, 2.0);
    /// ```
    fn get_elem(&self, index: usize) -> Option<f64> {
        match self.vals.get(index) {
            Some(v) => Some(*v),
            _ => None,
        }
    }

    /// Sets the element of the vector denoted by the index to the value val.
    ///
    /// # Examples
    /// ```
    /// use linalg::vector::Vector;
    /// use linalg::vector::RealVec;
    ///
    /// let mut a = RealVec::from_vec(vec![1.0, 2.0, 3.0]);
    /// let b = RealVec::from_vec(vec![1.0, 4.0, 3.0]);
    ///
    /// a.set_elem(1, 4.0).expect("Invalid index!");
    /// assert_eq!(a, b);
    /// ```
    fn set_elem(&mut self, index: usize, val: f64) -> Option<()> {
        match self.vals.get_mut(index) {
            Some(v) => *v = val,
            _ => return None,
        }
        Some(())
    }

    /// Gets the dimension of the vector.
    fn get_dim(&self) -> usize {
        self.dim
    }

    /// Clones a vector without moving its values.
    fn clone(&self) -> RealVec {
        RealVec {
            vals: Vec::from(&self.vals[..]),
            dim: self.dim,
            mutable: self.mutable,
        }
    }

    /// Clones a part of a RealVec. If the requested length is greater than the existing,
    /// the difference will be padded with zeroes.
    ///
    /// # Examples
    /// ```
    /// use linalg::vector::Vector;
    /// use linalg::vector::RealVec;
    ///
    /// let a = RealVec::from_vec(vec![-2.0, 4.0, 3.0]);
    /// let expected = RealVec::from_vec(vec![-2.0, 4.0, 3.0, 0.0, 0.0]);
    ///
    /// let b = a.clone_part(5);
    /// assert_eq!(b, expected);
    /// ```
    fn clone_part(&self, n: usize) -> RealVec {
        let reals;
        if n < self.dim {
            reals = Vec::from(&self.vals[0..n]);
        } else {
            let mut v = Vec::from(&self.vals[..]);
            for _ in 0..n - self.dim {
                v.push(0.0);
            }
            reals = v;
        }
        RealVec::from_vec(reals)
    }

    /// Adds two vectors and returns a new RealVec as a result.
    ///
    /// # Examples
    /// ```
    /// use linalg::vector::Vector;
    /// use linalg::vector::RealVec;
    ///
    /// let a = RealVec::from_vec(vec![1.0, 2.0, 3.0]);
    /// let b = RealVec::from_vec(vec![4.0, 5.0, 6.0]);
    /// let expected = RealVec::from_vec(vec![5.0, 7.0, 9.0]);
    ///
    /// let sum = a.add(&b).unwrap();
    /// assert_eq!(sum, expected);
    /// ```
    ///
    /// # Errors
    /// Returns an error if the vector dimensions are not equal.
    /// That is, it is not possible to add two vectors of different dimensions.
    fn add(&self, rhs: &RealVec) -> Result<RealVec> {
        self.check_dimensions(rhs)?;
        let result = self
            .vals
            .iter()
            .zip(rhs.values().iter())
            .map(|(a, b)| *a + *b)
            .collect::<Vec<f64>>();
        Ok(RealVec::from_vec(result))
    }

    /// Adds two vectors inplace in a way such that the value is stored in the first vector.
    ///
    /// # Examples
    /// ```
    /// use linalg::vector::Vector;
    /// use linalg::vector::RealVec;
    ///
    /// let mut a = RealVec::from_vec(vec![1.0, 2.0, 3.0]);
    /// let b = RealVec::from_vec(vec![4.0, 5.0, 6.0]);
    ///
    /// let expected = RealVec::from_vec(vec![5.0, 7.0, 9.0]);
    /// a.add_inplace(&b);
    ///
    /// assert_eq!(a, expected);
    /// ```
    ///
    /// # Errors
    /// Just like the regular add method, returns an error if the vector dimensions are not equal.
    fn add_inplace(&mut self, rhs: &RealVec) -> Result<()> {
        self.check_dimensions(rhs)?;
        self.vals
            .iter_mut()
            .zip(rhs.values().iter())
            .for_each(|(a, b)| *a = *a + *b);
        Ok(())
    }

    /// Subtracts two vectors and returns a new RealVec as a result.
    ///
    /// # Examples
    /// ```
    /// use linalg::vector::Vector;
    /// use linalg::vector::RealVec;
    ///
    /// let a = RealVec::from_vec(vec![1.0, 2.0, 3.0]);
    /// let b = RealVec::from_vec(vec![4.0, 5.0, 6.0]);
    /// let expected = RealVec::from_vec(vec![-3.0, -3.0, -3.0]);
    ///
    /// let sum = a.sub(&b).unwrap();
    /// assert_eq!(sum, expected);
    /// ```
    ///
    /// # Errors
    /// Returns an error if the vector dimensions are not equal.
    /// That is, it is not possible to subtract two vectors of different dimensions.
    fn sub(&self, rhs: &RealVec) -> Result<RealVec> {
        self.check_dimensions(rhs)?;
        let res = self
            .values()
            .iter()
            .zip(rhs.values().iter())
            .map(|(a, b)| *a - *b)
            .collect::<Vec<f64>>();
        Ok(RealVec::from_vec(res))
    }

    /// Subtracts two vectors inplace in a way such that the value is stored in the first vector.
    ///
    /// # Examples
    /// ```
    /// use linalg::vector::Vector;
    /// use linalg::vector::RealVec;
    ///
    /// let mut a = RealVec::from_vec(vec![1.0, 2.0, 3.0]);
    /// let b = RealVec::from_vec(vec![4.0, 5.0, 6.0]);
    /// let expected = RealVec::from_vec(vec![-3.0, -3.0, -3.0]);
    ///
    /// a.sub_inplace(&b).expect("Invalid dimensions");
    /// assert_eq!(a, expected);
    /// ```
    ///
    /// # Errors
    /// Just like the regular sub method, returns an error if the vector dimensions are not equal.
    fn sub_inplace(&mut self, rhs: &RealVec) -> Result<()> {
        self.check_dimensions(rhs)?;
        self.vals
            .iter_mut()
            .zip(rhs.values().iter())
            .for_each(|(a, b)| *a = *a - *b);
        Ok(())
    }

    /// Multiplies the vector by a scalar value and returns the result as a new vector.
    ///
    /// # Examples
    /// ```
    /// use linalg::vector::Vector;
    /// use linalg::vector::RealVec;
    ///
    /// let a = RealVec::from_vec(vec![1.0, 2.0, 3.0]);
    /// let expected_pos = RealVec::from_vec(vec![10.0, 20.0, 30.0]);
    /// let expected_neg = RealVec::from_vec(vec![-10.0, -20.0, -30.0]);
    /// let expected_zer = RealVec::from_vec(vec![0.0, 0.0, 0.0]);
    ///
    /// let a_1 = a.scale(10.0);
    /// let a_2 = a_1.scale(-1.0);
    /// let a_3 = a_2.scale(0.0);
    ///
    /// assert_eq!(a_1, expected_pos);
    /// assert_eq!(a_2, expected_neg);
    /// assert_eq!(a_3, expected_zer);
    /// ```
    fn scale(&self, coef: f64) -> RealVec {
        let res = self
            .values()
            .iter()
            .map(|a| (*a) * coef)
            .collect::<Vec<f64>>();
        RealVec::from_vec(res)
    }

    /// Multiplies the vector by a scalar value and mutates the original vector.
    ///
    /// # Examples
    /// ```
    /// use linalg::vector::Vector;
    /// use linalg::vector::RealVec;
    ///
    /// let mut a = RealVec::from_vec(vec![1.0, 2.0, 3.0]);
    ///
    /// let expected_pos = RealVec::from_vec(vec![10.0, 20.0, 30.0]);
    /// let expected_neg = RealVec::from_vec(vec![-10.0, -20.0, -30.0]);
    /// let expected_zer = RealVec::from_vec(vec![0.0, 0.0, 0.0]);
    ///
    /// a.scale_inplace(10.0);
    /// assert_eq!(a, expected_pos);
    ///
    /// a.scale_inplace(-1.0);
    /// assert_eq!(a, expected_neg);
    ///
    /// a.scale_inplace(0.0);
    /// assert_eq!(a, expected_zer);
    /// ```
    fn scale_inplace(&mut self, coef: f64) {
        self.vals.iter_mut().for_each(|a| *a = (*a) * coef);
    }

    /// Calculates the L-2 norm of a vector.
    ///
    /// # Examples
    /// ```
    /// use linalg::vector::Vector;
    /// use linalg::vector::RealVec;
    ///
    /// let a = RealVec::from_vec(vec![1.0, 2.0, 3.0, 4.0, 5.0]);
    /// let b = RealVec::from_vec(vec![-1.0, -2.0, -3.0, -4.0, -5.0]);
    ///
    /// assert_eq!(a.norm(), b.norm());
    /// ```
    fn norm(&self) -> f64 {
        self.values()
            .iter()
            .fold(0.0, |sq_sum, i| sq_sum + i.powi(2))
            .sqrt()
    }

    /// Normalizes the vector and returns the result as a new vector.
    ///
    /// # Examples
    /// ```
    /// use linalg::vector::Vector;
    /// use linalg::vector::RealVec;
    ///
    /// let a = RealVec::from_vec(vec![1.0, 2.0, 3.0, 4.0, 5.0]);
    /// let b = a.normalize();
    ///
    /// assert!(b.norm() - 1.0 < 1e-6);
    ///
    /// let n = 7.4162;
    /// let c = RealVec::from_vec(vec![1.0/n, 2.0/n, 3.0/n, 4.0/n, 5.0/n]);
    /// assert_eq!(b, c);
    /// ```
    fn normalize(&self) -> RealVec {
        self.scale(1.0 / self.norm())
    }

    /// Normalizes the vector while mutating it.
    ///
    /// # Examples
    /// ```
    /// use linalg::vector::Vector;
    /// use linalg::vector::RealVec;
    ///
    /// let mut a = RealVec::from_vec(vec![1.0, 2.0, 3.0, 4.0, 5.0]);
    /// a.normalize_inplace();
    ///
    /// let n = 7.4162;
    /// let c = RealVec::from_vec(vec![1.0/n, 2.0/n, 3.0/n, 4.0/n, 5.0/n]);
    ///
    /// assert_eq!(a, c);
    /// ```
    fn normalize_inplace(&mut self) {
        self.scale_inplace(1.0 / self.norm())
    }

    /// Assumes that the vector is in homogeneous space and returns its Cartesian space counterpart.
    /// Provided that the input vector is d-dimensional, the resulting Cartesian vector will be (d-1)-dimensional
    /// and its values will be scaled reversely proportionate to its last entry, that is, its homogeneous coordinate.
    ///
    /// # Examples
    /// ```
    /// use linalg::vector::Vector;
    /// use linalg::vector::RealVec;
    ///
    /// let h = RealVec::from_vec(vec![1.0, 2.0, 3.0, 4.0, 5.0]);
    /// let cartesian = h.from_homogeneous().unwrap();
    /// let expected = RealVec::from_vec(vec![1.0/5.0, 2.0/5.0, 3.0/5.0, 4.0/5.0]);
    ///
    /// assert_eq!(cartesian, expected);
    /// ```
    ///
    /// # Errors
    /// Returns an error if its dimension is smaller or equal to 1, that is, if the Cartesian vector is non-dimensional.
    fn from_homogeneous(&self) -> Result<RealVec> {
        if self.vals.len() <= 1 {
            return Err(MathError::DimensionError);
        }
        let len = self.vals.len();
        let last = *(self.vals.get(self.values().len() - 1).unwrap());
        let real_space_pts = self
            .values()
            .iter()
            .take(len - 1)
            .map(|a| (*a) / last)
            .collect::<Vec<f64>>();
        Ok(RealVec::from_vec(real_space_pts))
    }

    /// Calculates the dot product of two vectors.
    ///
    /// # Examples
    /// ```
    /// use linalg::vector::Vector;
    /// use linalg::vector::RealVec;
    ///
    /// let a = RealVec::from_vec(vec![12.0, 20.0]);
    /// let b = a.clone();
    /// let c = RealVec::from_vec(vec![16.0, -5.0]);
    /// let perpendicular = RealVec::from_vec(vec![5.0, -3.0]);
    ///
    /// assert_eq!(a.dot(&b), 544.0);
    /// assert_eq!(a.dot(&c), 92.0);
    /// assert_eq!(a.dot(&perpendicular), 0.0);
    /// ```
    fn dot(&self, other: &RealVec) -> f64 {
        self.values()
            .iter()
            .zip(other.values().iter())
            .fold(0.0, |sq_sum, (x, y)| sq_sum + (*x) * (*y))
    }

    /// Calculates the cross product of two 3-D vectors. This is currently not implemented for multidimensional vectors.
    ///
    /// # Examples
    /// ```
    /// use linalg::vector::Vector;
    /// use linalg::vector::RealVec;
    ///
    /// let a = RealVec::from_vec(vec![8.0, 3.0, -1.0]);
    /// let b = a.clone();
    /// let c1 = a.cross(&b).unwrap();
    /// assert_eq!(c1, RealVec::from_vec(vec![0.0, 0.0, 0.0]));
    ///
    /// let c = RealVec::from_vec(vec![1.0, -1.0, 2.0]);
    /// let expected = RealVec::from_vec(vec![5.0, -17.0, -11.0]);
    /// let c2 = a.cross(&c).unwrap();
    /// assert_eq!(c2, expected);
    /// ```
    ///
    /// # Errors
    /// Returns an error if the vector is not in 3-D space.
    fn cross(&self, other: &RealVec) -> Result<RealVec> {
        if self.vals.len() != 3 && other.vals.len() != 3 {
            return Err(MathError::DimensionError);
        }
        let a = &self.vals[0..=2];
        let b = &other.values()[0..=2];
        let p1 = a[1] * b[2] - a[2] * b[1];
        let p2 = a[0] * b[2] - a[2] * b[0];
        let p3 = a[0] * b[1] - a[1] * b[0];
        let p = RealVec::from_vec(vec![p1, -1.0 * p2, p3]);
        Ok(p)
    }

    /// Calculates the cosine of the angle between the given two vectors.
    ///
    /// # Examples
    /// ```
    /// use linalg::vector::Vector;
    /// use linalg::vector::RealVec;
    ///
    /// let a = RealVec::from_vec(vec![12.0, 20.0]);
    /// let perpendicular = RealVec::from_vec(vec![5.0, -3.0]);
    /// assert_eq!(a.cosine(&perpendicular), 0.0);
    /// ```
    fn cosine(&self, other: &RealVec) -> f64 {
        self.dot(other) / (self.norm() * other.norm())
    }

    /// Returns a reference to the vector's values.
    fn elems(&self) -> &Vec<f64> {
        &self.vals
    }

    /// Returns a mutable reference to the vector's values.
    fn elems_mut(&mut self) -> &mut Vec<f64> {
        &mut self.vals
    }

    /// Creates a RealVec from an existing row matrix.
    ///
    /// # Examples
    /// ```
    /// use linalg::vector::{Vector, RealVec};
    /// use linalg::matrix::{Matrix, RealMat};
    ///
    /// let m = RealMat::from_vec(
    ///     vec![vec![12.0, 20.0, 30.0]]
    /// ).unwrap();
    ///
    /// let expected = RealVec::from_vec(vec![12.0, 20.0, 30.0]);
    /// let vec = RealVec::from_row_matrix(&m).unwrap();
    /// assert_eq!(vec, expected);
    /// ```
    ///
    /// # Errors
    /// Returns an error if the matrix is not a row matrix.
    fn from_row_matrix(m: &impl Matrix) -> Result<RealVec> {
        if m.dim_rows() != 1 {
            return Err(MathError::DimensionError);
        }
        let cols = m.get_row(0).unwrap();
        let mut res = Vec::with_capacity(cols.len());

        for elem in cols.iter() {
            res.push(elem.clone());
        }
        Ok(RealVec::from_vec(res))
    }

    /// Creates a RealVec from an existing column matrix.
    ///
    /// # Examples
    /// ```
    /// use linalg::vector::{Vector, RealVec};
    /// use linalg::matrix::{Matrix, RealMat};
    ///
    /// let m_err = RealMat::from_vec(
    ///     vec![vec![12.0, 20.0, 30.0]]
    /// ).unwrap();
    ///
    /// let error = RealVec::from_col_matrix(&m_err);
    /// assert!(!error.is_ok());
    ///
    /// let m_ok = RealMat::from_vec(
    ///     vec![
    ///         vec![12.0],
    ///         vec![20.0],
    ///         vec![30.0]
    ///     ]
    /// ).unwrap();
    ///
    /// let expected = RealVec::from_vec(vec![12.0, 20.0, 30.0]);
    /// let vec = RealVec::from_col_matrix(&m_ok).unwrap();
    /// assert_eq!(vec, expected);
    /// ```
    ///
    /// # Errors
    /// Returns an error if the matrix is not a column matrix.
    fn from_col_matrix(m: &impl Matrix) -> Result<RealVec> {
        if m.dim_cols() != 1 {
            return Err(MathError::DimensionError);
        }
        let rows = m.values();
        let mut res = Vec::with_capacity(rows.len());

        for row in rows.iter() {
            let elem = row.get(0).unwrap();
            res.push(elem.clone());
        }
        Ok(RealVec::from_vec(res))
    }
}
