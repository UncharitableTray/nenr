extern crate linalg;
extern crate rand;

use std::error::Error;
use std::fs::File;
use std::io::prelude::*;

use crate::anfis::AnfisExample;
use crate::evolnet::{EvolNet, EvolnetExample};
use rand::seq::SliceRandom;
use rand::{thread_rng, Rng};

pub use linalg::matrix::RealMat;

pub fn sigmoid(x: f64) -> f64 {
    1.0 / (1.0 + (-x).exp())
}

pub fn sigmoid_param(x: f64, a: f64, b: f64) -> f64 {
    let prod = (x - a) * b;
    1.0 / (1.0 + prod.exp())
}

pub fn shuffle_dataset<'a>(
    xs: &'a [RealMat],
    ys: &'a [RealMat],
) -> (Vec<&'a RealMat>, Vec<&'a RealMat>) {
    assert_eq!(xs.len(), ys.len());
    let len = xs.len();
    let mut indices: Vec<usize> = (0..len).collect();
    let ind_slice: &mut [usize] = &mut indices;
    let mut rng = thread_rng();

    ind_slice.shuffle(&mut rng);
    let mut new_xs = Vec::with_capacity(len);
    let mut new_ys = Vec::with_capacity(len);

    for i in 0..len {
        new_xs.push(&xs[ind_slice[i]]);
        new_ys.push(&ys[ind_slice[i]]);
    }
    (new_xs, new_ys)
}

pub fn create_anfis_examples() -> Vec<AnfisExample> {
    let a = 4;
    let mut dset = Vec::new();
    for x in -a..=a {
        for y in -a..=a {
            let x = x as f64;
            let y = y as f64;
            let eg = (vec![x, y], anfis_function(x, y));
            dset.push(eg);
        }
    }
    dset
}

pub fn anfis_function(x: f64, y: f64) -> f64 {
    let a1 = (x - 1.0).powi(2);
    let a2 = (y + 2.0).powi(2);
    let a3 = -5.0 * x * y + 3.0;
    let a4 = (x / 5.0).cos().powi(2);
    (a1 + a2 + a3) * a4
}

pub fn create_vec_rand(len: usize, range: f64) -> Vec<f64> {
    let mut rng = thread_rng();
    let mut new_vec = Vec::with_capacity(len);

    for _ in 0..len {
        new_vec.push(rng.gen_range(-range, range));
    }
    new_vec
}

pub fn create_vec_empty(len: usize) -> Vec<f64> {
    vec![0.0; len]
}

pub fn create_evolnet_dataset(path: &str) -> Vec<EvolnetExample> {
    let mut file = match File::open(path) {
        Ok(f) => f,
        Err(why) => panic!("Could not open {}: {}", path, why.description()),
    };
    let mut contents = String::new();
    file.read_to_string(&mut contents)
        .expect("Unable to read from file");

    let mut dataset = Vec::new();
    for line in contents.split("\n") {
        if line.len() == 0 {
            break;
        }
        let nums: Vec<f64> = line
            .split("\t")
            .map(|s| s.trim().parse::<f64>().unwrap())
            .collect();
        let example = (Vec::from(&nums[0..=1]), Vec::from(&nums[2..]));
        dataset.push(example);
    }

    dataset
}

pub fn write_trained_parameters(net: &EvolNet, path: &str) {
    let params = net.get_params();
    let mut file = File::create(path).expect("Unable to open trained params output file.");

    for i in 0..8 {
        let mut nums = Vec::new();
        writeln!(
            &mut nums,
            "{} {} {} {}",
            params[i * 4],
            params[i * 4 + 2],
            params[i * 4 + 1],
            params[i * 4 + 3]
        )
        .unwrap();
        file.write(&nums)
            .expect("Unable to write to params output file...");
    }
}
