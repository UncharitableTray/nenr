extern crate linalg;
extern crate rand;

use crate::util::sigmoid;
use linalg::matrix::{Matrix, RealMat};
use rand::{thread_rng, Rng};

pub trait Layer {
    // input je row matrix jednog primjera
    fn forward_pass(&mut self, input: &RealMat) -> RealMat;

    fn backward_pass(&mut self, error: &RealMat) -> RealMat;

    fn update_weight(&mut self, learning_rate: f64);
}

#[derive(Debug)]
pub struct LinearLayer {
    pub weights: RealMat,
    pub bias: RealMat,
    pub last_input: Option<RealMat>,
    pub last_output: Option<RealMat>,
    pub last_error: Option<RealMat>,
}

impl LinearLayer {
    pub fn new(input_size: usize, output_size: usize) -> LinearLayer {
        let mut cols = Vec::new();
        let mut rng = thread_rng();
        for _ in 0..input_size {
            let mut rows = Vec::new();
            for _ in 0..output_size {
                rows.push(rng.gen_range(-1.0, 1.0));
            }
            cols.push(rows);
        }
        let weights = RealMat::from_vec(cols).unwrap();

        let mut bias_vec = Vec::new();
        for _ in 0..output_size {
            bias_vec.push(rng.gen_range(-1.0, 1.0));
        }
        let bias = RealMat::from_vec(vec![bias_vec]).unwrap();
        LinearLayer {
            weights: weights,
            bias: bias,
            last_input: None,
            last_output: None,
            last_error: None,
        }
    }
}

impl Layer for LinearLayer {
    fn forward_pass(&mut self, input: &RealMat) -> RealMat {
        self.last_input = Some(input.clone());
        let res = input
            .mul(&self.weights)
            .expect("Multiplying error in forward pass")
            .add(&self.bias)
            .expect("Adding error in forward pass.");
        println!("Forward pass output: {}", res);
        res
    }

    fn backward_pass(&mut self, error: &RealMat) -> RealMat {
        self.last_error = Some(error.clone());
        println!("Backward pass input error: {}", error);
        let transposed = self.weights.transpose().expect("Unable to transpose.");
        println!("transposed: {}, error: {}", transposed, error);
        let res = error
            .mul(&transposed)
            .expect("Error in backward pass multiplication");
        println!("Backward pass output: {}", res);
        res
    }

    fn update_weight(&mut self, learning_rate: f64) {
        let inputs_transposed = self.last_input.as_ref().unwrap().transpose().unwrap();
        let errors = self.last_error.as_ref().unwrap();

        println!("Weights before update: {}", self.weights);
        let mut mult = inputs_transposed
            .mul(&errors)
            .expect("Error in update multiplication");
        mult = mult.scale(learning_rate).unwrap();
        self.weights
            .sub_inplace(&mult)
            .expect("Error in update subtraction");
        println!("Weights after update: {}", self.weights);

        println!("Bias before update: {}", self.bias);
        println!("Updating with errors: {}", errors);
        let vals = &mut self.bias.values_mut()[0];
        for i in 0..vals.len() {
            let sum = learning_rate
                * errors
                    .get_col(i)
                    .unwrap()
                    .iter()
                    .fold(0.0, |acc, x| acc + x);
            vals[i] = vals[i] - learning_rate * sum;
        }
        println!("Bias after update: {}", self.bias);
    }
}

pub struct SigmoidLayer {
    pub size: usize,
    pub last_output: Option<RealMat>,
}

impl SigmoidLayer {
    pub fn new(size: usize) -> SigmoidLayer {
        SigmoidLayer {
            size: size,
            last_output: None,
        }
    }
}

impl Layer for SigmoidLayer {
    fn forward_pass(&mut self, input: &RealMat) -> RealMat {
        let mut output = input.clone();
        for i in 0..output.dim_rows() {
            for j in 0..output.dim_cols() {
                output
                    .set_elem(i, j, sigmoid(input.get_elem(i, j).unwrap()))
                    .unwrap();
            }
        }
        self.last_output = Some(output.clone());
        output
    }

    fn backward_pass(&mut self, error: &RealMat) -> RealMat {
        let mut output = error.clone();
        for i in 0..error.dim_rows() {
            for j in 0..error.dim_cols() {
                let e = error.get_elem(i, j).unwrap();
                let o = self.last_output.as_ref().unwrap().get_elem(i, j).unwrap();
                output.set_elem(i, j, e * o * (1.0 - o)).unwrap();
            }
        }
        println!("backprop_err: {}", output);
        output
    }

    fn update_weight(&mut self, _: f64) {}
}
