import numpy as np
import sys
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

filename = sys.argv[1]
lines = []
matrix = []

with open(filename, 'r') as in_file:
    for line in in_file.readlines():
        if line is not '\n':
            splitter = line.strip().split(' ')
            nums = list(map(float, splitter))
            lines.append(nums)

arr = np.array(lines)

fig = plt.figure()
ax = fig.add_subplot(111)
ax.set(title=(filename[:-4] + "_ylim"))
ax.plot(arr[:, 0], arr[:, 1])
ax.set_ylim([0, 20])

plt.savefig("img_" + filename[:-4] + "_ylim")
#plt.show()
