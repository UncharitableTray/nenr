import numpy as np
import sys
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

filename = sys.argv[1]
lines = []
matrix = []

with open(filename, 'r') as in_file:
    for line in in_file.readlines():
        if line is not '\n':
            splitter = line.strip().split(' ')
            nums = list(map(float, splitter))
            lines.append(nums)

arr = np.array(lines)

#print(arr[:, 2])

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.set(title=filename[:-4])
ax.plot_trisurf(arr[:, 0], arr[:, 1], arr[:, 2], cmap=plt.cm.Spectral)

# ax.plot(m[:, 0], m[:, 1])
#    ax.plot(m[:, 0], m[:, 2])
#    ctr += 1

ax.view_init(45, 45)
plt.savefig("img_" + filename[:-4])
#plt.show()
