import numpy as np
import matplotlib.pyplot as plt

def t1_output(x, w, s):
    return 1 / (1 + abs(x - w) / abs(s))

x_domain = np.linspace(-8, 10, 100)
test_s = [0.25, 1, 4]

fig = plt.figure()
ax = fig.add_subplot(111)
ax.set(title="Type 1 output")

for s in test_s:
    out = t1_output(x_domain, 2, s)
    ax.plot(x_domain, out)

ax.legend(test_s)
plt.savefig("img_type1_output")