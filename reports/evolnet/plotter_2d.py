import numpy as np
import sys
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

def filter_by_class(xs, ys, y_ind):
    new_xs = []
    new_ys = []
    for i in range(len(xs)):
        if ys[i][y_ind] == 1:
            new_xs.append(xs[i])
            new_ys.append(ys[i])

    return np.array(new_xs), np.array(new_ys)

filename = sys.argv[1]
xs = []
ys = []

with open(filename, 'r') as in_file:
    for line in in_file.readlines():
        if line is not '\n':
            splitter = line.strip().split('\t')
            x = list(map(float, splitter[0:2]))
            y = list(map(float, splitter[2:]))
            xs.append(x)
            ys.append(y)


x_arr_1, y_arr_1 = filter_by_class(xs, ys, 0)
x_arr_2, y_arr_2 = filter_by_class(xs, ys, 1)
x_arr_3, y_arr_3 = filter_by_class(xs, ys, 2)

fig = plt.figure()
ax = fig.add_subplot(111)
ax.set(title=(filename[:-4]))

ax.scatter(x_arr_1[:, 0], x_arr_1[:, 1], marker="o")
ax.scatter(x_arr_2[:, 0], x_arr_2[:, 1], marker="+")
ax.scatter(x_arr_3[:, 0], x_arr_3[:, 1], marker="x")

ax.legend([0, 1, 2])
plt.savefig("img_" + filename[:-4])
