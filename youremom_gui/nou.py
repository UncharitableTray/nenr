from tkinter import *
import numpy as np

root = Tk()
root.title("you're mom")

cls = StringVar()
entry = Entry(root, textvariable=cls)
entry.pack()

M = 10
classes = 5
xs = []
ys = []

def normalize():
    x = np.array(xs).reshape((-1, 1))
    y = np.array(ys).reshape((-1, 1))
    koord = np.hstack([x, y])

    avg = np.average(koord, axis=0)
    koord = koord - avg
    absmax = np.max(np.absolute(koord), axis=0)
    m = np.max(absmax)

    return koord / m

def distance(coords):
    d = coords[:-1] - coords[1:]
    d = d ** 2
    d = np.sum(d, axis=1)
    d = np.sqrt(d)
    d = np.sum(d)
    return d

def sample(coords, d):
    step = d / (M - 1)
    csum = 0
    current_limit = 0
    representative = []

    for i in range(len(coords) - 1):
        csum += np.sum(np.sqrt(np.sum((coords[i] - coords[i + 1]) ** 2)))
        if csum >= current_limit:
            current_limit += step
            representative.append(coords[i])
    if len(representative) < 10:
        representative.append(coords[-1])

    return np.array(representative)

def format(pts, label):
    bob = ''
    for pt in pts:
        bob += str(pt[0]) + ' ' + str(pt[1]) + ' '

    labels = [0] * classes 
    labels[int(label)] = 1

    bob = bob[:-1] + '|'
    bob += str(labels)

    return bob

def release(event):
    global xs
    global ys

    normalized = normalize()
    #print(normalized)
    dist = distance(normalized)
    #print(dist)
    points = sample(normalized, dist)
    #print(points)

    l = entry.get()
    out_string = format(points, l)
    print(out_string)

    canvas.delete('all')
    xs = []
    ys = []

def callback(event):
    x1, y1 = event.x - 1, event.y - 1
    x2, y2 = event.x + 1, event.y + 1
    canvas.create_oval(x1, y1, x2, y2)
    xs.append(event.x)
    ys.append(event.y)


frame = Frame(root, bg='white', width=500, height=500)
frame.pack()

canvas = Canvas(frame, bg='white', width=500, height=500)
canvas.bind("<B1-Motion>", callback)
canvas.bind("<ButtonRelease-1>", release)
canvas.pack()

root.mainloop()
