use super::domain::{CompositeDomain, Domain, Element};
use super::errors::FuzzyError;
use super::fset::{FuzzySet, MutableFset};

type Result<T> = std::result::Result<T, FuzzyError>;

/// Checks whether the domain over a fuzzy set is a cartesian
/// product of an universal set with itself.
pub fn is_u_times_u_relation<S: FuzzySet<T>, T: Domain>(rel: &S) -> Result<bool> {
    let domain = rel.get_domain();
    if domain.comp_count() != 2 {
        return Err(FuzzyError::DomainError);
    }
    let d1 = domain.comp(0).unwrap();
    let d2 = domain.comp(1).unwrap();
    Ok(d1.start == d2.start && d1.stop == d2.stop)
}

/// Checks whether the set is a symmetric fuzzy relation over a U x U domain.
pub fn is_symmetric<S: FuzzySet<T>, T: Domain>(rel: &S) -> Result<bool> {
    let domain = rel.get_domain();
    match is_u_times_u_relation(rel) {
        Ok(_) => (),
        Err(e) => return Err(e),
    }

    let d1 = domain.comp(0).unwrap();
    let n = d1.cardinality();
    let mut symmetric = true;

    for i in 0..n {
        for j in i..n {
            let fst = domain.elem_get(n * i + j).unwrap();
            let snd = domain.elem_get(i + n * j).unwrap();
            if rel.get_val_at(&fst)? != rel.get_val_at(&snd)? {
                symmetric = false;
            }
        }
    }
    Ok(symmetric)
}

/// Checks whether the set is a reflexive fuzzy relation over a U x U domain.
pub fn is_reflexive<S: FuzzySet<T>, T: Domain>(rel: &S) -> Result<bool> {
    let domain = rel.get_domain();
    match is_u_times_u_relation(rel) {
        Ok(_) => (),
        Err(e) => return Err(e),
    }

    let d1 = domain.comp(0).unwrap();
    let n = d1.cardinality();
    let mut reflexive = true;
    for i in 0..n {
        let elem = domain.elem_get(n * i + i).unwrap();
        let val = rel.get_val_at(&elem)?;
        if (val - 1.0).abs() > 1e-8 {
            reflexive = false;
        }
    }
    Ok(reflexive)
}

/// Checks whether the set is a fuzzy relation
/// over a U x U domain whcih is max-min transitive.
pub fn is_max_min_transitive<S: FuzzySet<T>, T: Domain>(rel: &S) -> Result<bool> {
    let domain = rel.get_domain();
    match is_u_times_u_relation(rel) {
        Ok(_) => (),
        Err(e) => return Err(e),
    }
    let u = domain.comp(0).unwrap();
    for (x, z) in iproduct!(u.clone().into_iter(), u.clone().into_iter()) {
        let mut max = 0.0;
        let comp_x = x.comp_value(0);
        let comp_z = z.comp_value(0);
        let mu_xz = rel.get_val_at(&Element::from_vec(vec![comp_x, comp_z]))?;

        for y in u.clone().into_iter() {
            let comp_y = y.comp_value(0);
            let fst = Element::from_vec(vec![comp_x, comp_y]);
            let snd = Element::from_vec(vec![comp_y, comp_z]);
            let min = rel.get_val_at(&fst)?.min(rel.get_val_at(&snd)?);

            if min > max {
                max = min;
            }
        }
        if mu_xz < max {
            return Ok(false);
        }
    }
    Ok(true)
}

pub fn compose_binary<S: FuzzySet<T>, T: Domain>(
    r1: &S,
    r2: &S,
) -> Result<MutableFset<CompositeDomain>> {
    check_domains(r1, r2)?;
    let d1 = r1.get_domain();
    let d2 = r2.get_domain();

    let a = d1.comp(0).unwrap();
    let b = d1.comp(1).unwrap();
    let c = d2.comp(1).unwrap();

    let res_domain = Domain::combine(a, c);
    let mut res = MutableFset::new(res_domain.clone());

    //println!("Dimensions: {}, {}, {}", dim_a, dim_b, dim_c);
    //println!("d1 has {} elems, d2 has {} elems.", d1.cardinality(), d2.cardinality());
    for (x, z) in iproduct!(a.clone().into_iter(), c.clone().into_iter()) {
        let comp_x = x.comp_value(0);
        let comp_z = z.comp_value(0);

        let mut max = 0.0;
        for y in b.clone().into_iter() {
            let comp_y = y.comp_value(0);
            let fst = Element::from_vec(vec![comp_x, comp_y]);
            let snd = Element::from_vec(vec![comp_y, comp_z]);
            let min = r1.get_val_at(&fst)?.min(r2.get_val_at(&snd)?);

            if min > max {
                max = min;
            }
        }
        let e = Element::from_vec(vec![comp_x, comp_z]);
        res.set_value(&e, max)?;
    }
    Ok(res)
}

fn check_domains<S: FuzzySet<T>, T: Domain>(r1: &S, r2: &S) -> Result<()> {
    let d1 = r1.get_domain();
    let d2 = r2.get_domain();
    if d1.comp_count() != 2 || d2.comp_count() != 2 {
        return Err(FuzzyError::DomainError);
    }

    if d1.comp_count() == 1 {
        let c1 = d1.comp(0).unwrap();
        let c2 = d2.comp(0).unwrap();

        if c1.cardinality() != c2.cardinality() {
            return Err(FuzzyError::DomainError);
        }
    }
    Ok(())
}

pub fn is_fuzzy_equivalence<S: FuzzySet<T>, T: Domain>(rel: &S) -> Result<bool> {
    Ok(is_symmetric(rel)? && is_reflexive(rel)? && is_max_min_transitive(rel)?)
}
