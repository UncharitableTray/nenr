pub mod domain;
pub mod errors;
pub mod fset;
pub mod inference;
pub mod ops;
pub mod relations;
pub mod util;

#[macro_use]
extern crate itertools;
