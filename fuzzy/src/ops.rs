use super::domain::Domain;
use super::fset::{FuzzySet, MutableFset};
use std::fmt::Debug;

pub trait IntUnaryFn: Debug {
    fn value_at(&self, x: i32) -> f64;
}

pub trait DblUnaryFn {
    fn value_at(&self, x: f64) -> f64;
}

pub trait BinaryFn {
    fn value_at(&self, x: f64, y: f64) -> f64;
}

pub mod unary_ops {
    use super::IntUnaryFn;

    #[derive(Debug)]
    pub struct LFun {
        alpha: i32,
        beta: i32,
    }

    impl LFun {
        pub fn new(alpha_: i32, beta_: i32) -> LFun {
            LFun {
                alpha: alpha_,
                beta: beta_,
            }
        }
    }

    impl IntUnaryFn for LFun {
        fn value_at(&self, x: i32) -> f64 {
            if x < self.alpha {
                1.0
            } else if x >= self.alpha && x < self.beta {
                ((self.beta - x) as f64) / ((self.beta - self.alpha) as f64)
            } else {
                0.0
            }
        }
    }

    #[derive(Debug)]
    pub struct GammaFun {
        alpha: i32,
        beta: i32,
    }

    impl GammaFun {
        pub fn new(alpha_: i32, beta_: i32) -> GammaFun {
            GammaFun {
                alpha: alpha_,
                beta: beta_,
            }
        }
    }

    impl IntUnaryFn for GammaFun {
        fn value_at(&self, x: i32) -> f64 {
            if x < self.alpha {
                0.0
            } else if x >= self.alpha && x < self.beta {
                (x - self.alpha) as f64 / (self.beta - self.alpha) as f64
            } else {
                1.0
            }
        }
    }

    #[derive(Debug)]
    pub struct LambdaFun {
        alpha: i32,
        beta: i32,
        gamma: i32,
    }

    impl LambdaFun {
        pub fn new(alpha_: i32, beta_: i32, gamma_: i32) -> LambdaFun {
            LambdaFun {
                alpha: alpha_,
                beta: beta_,
                gamma: gamma_,
            }
        }
    }

    impl IntUnaryFn for LambdaFun {
        fn value_at(&self, x: i32) -> f64 {
            if x < self.alpha {
                0.0
            } else if x >= self.alpha && x < self.beta {
                (x - self.alpha) as f64 / (self.beta - self.alpha) as f64
            } else if x >= self.beta && x < self.gamma {
                (self.gamma - x) as f64 / (self.gamma - self.beta) as f64
            } else {
                0.0
            }
        }
    }

    #[derive(Debug)]
    pub struct OneFun {}

    impl OneFun {
        pub fn new() -> OneFun {
            OneFun {}
        }
    }

    impl IntUnaryFn for OneFun {
        fn value_at(&self, _: i32) -> f64 {
            1.0
        }
    }
}

pub fn real_unary_operate<U: DblUnaryFn, D: Domain>(
    set: &MutableFset<D>,
    fun: &U,
) -> MutableFset<D> {
    let mut new_set = (*set).clone();
    for x in new_set.get_domain().clone() {
        new_set
            .set_value(
                &x,
                fun.value_at(
                    set.get_val_at(&x)
                        .expect("Nonexistent element, check the boundaries or something."),
                ),
            )
            .unwrap();
    }
    new_set
}

pub fn real_binary_operate<U: BinaryFn, D: Domain>(
    fst: &MutableFset<D>,
    snd: &MutableFset<D>,
    fun: &U,
) -> MutableFset<D> {
    let mut new_set = MutableFset::new(fst.get_domain().clone());

    new_set.get_domain().clone().into_iter().for_each(|x| {
        let a = fst
            .get_val_at(&x)
            .expect("Invalid domain range for first set.");
        let b = snd
            .get_val_at(&x)
            .expect("Invalid domain range for second set.");
        let res = fun.value_at(a, b);
        new_set.set_value(&x, res).unwrap();
    });
    new_set
}

/// Operations over fuzzy sets.
pub mod binary_ops {
    use super::{BinaryFn, DblUnaryFn};

    pub struct ZadehAnd {}

    impl BinaryFn for ZadehAnd {
        fn value_at(&self, x: f64, y: f64) -> f64 {
            x.min(y)
        }
    }

    pub struct ZadehOr {}

    impl BinaryFn for ZadehOr {
        fn value_at(&self, x: f64, y: f64) -> f64 {
            x.max(y)
        }
    }

    pub struct ZadehNot {}

    impl DblUnaryFn for ZadehNot {
        fn value_at(&self, x: f64) -> f64 {
            1.0 - x
        }
    }

    pub struct HamacherTNorm {
        pub param: f64,
    }

    impl BinaryFn for HamacherTNorm {
        fn value_at(&self, x: f64, y: f64) -> f64 {
            assert!(self.param >= 0.0);
            let numer = x * y;
            let denom = self.param + (1.0 - self.param) * (x + y - x * y);
            numer / denom
        }
    }

    pub struct HamacherSNorm {
        pub param: f64,
    }

    impl BinaryFn for HamacherSNorm {
        fn value_at(&self, x: f64, y: f64) -> f64 {
            assert!(self.param >= 0.0);
            let numer = x + y - (2.0 - self.param) * x * y;
            let denom = 1.0 - (1.0 - self.param) * x * y;
            numer / denom
        }
    }
}
