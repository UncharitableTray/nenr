#[derive(Debug)]
pub enum FuzzyError {
    IndexError,
    IntervalError,
    CodomainError,
    DomainError,
}
