use std::fmt::Debug;

pub trait Unit: Clone {
    type Chromosome: Clone + Debug + PartialEq;
    type Fitness;

    fn get_chromo(&self) -> &Self::Chromosome;

    fn get_fitness(&self) -> &Self::Fitness;

    fn get_fitness_scalar(&self) -> f64;

    fn update_chromosome(&mut self, chromo: Self::Chromosome);

    fn update_fitness(&mut self, fit: f64);
}
