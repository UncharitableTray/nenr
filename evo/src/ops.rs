use crate::fitness::Fitness;
use crate::population::Population;
use crate::unit::Unit;

use rand::Rng;

pub trait CrossoverOp<G: Unit> {
    fn crossover<R>(&self, parents: Vec<G>, rng: &mut R) -> Vec<G>
    where
        R: Rng + Sized;
}

pub trait MutationOp<G: Unit> {
    fn mutate<R>(&self, unit: G, rng: &mut R) -> G
    where
        R: Rng + Sized;
}

pub trait ReinsertionOp<G, F>
where
    G: Unit,
    F: Fitness,
{
    fn combine<R>(&self, offspring: &mut Vec<G>, pop: &Population<G>, rng: &mut R) -> Vec<G>
    where
        R: Rng + Sized;
}

/// Selection operators, used to create a selection algorithm
pub trait SelectionOp<G, F>
where
    G: Unit,
    F: Fitness,
{
    fn select_from<R>(&self, current_gen: &Population<G>, rng: &mut R) -> Vec<G>
    where
        R: Rng + Sized;
}
