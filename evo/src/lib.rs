pub mod algorithm;
pub mod fitness; //Fitness, Fitness Function
pub mod genetic;
pub mod ops;
pub mod population; //Population
pub mod real;
pub mod unit; //Unit, chromosome
